---
headless: true
---
- [Installation Guide]({{< relref "/docs/Installation/Installation.md" >}})
    - [Terms of Use]({{< relref "/docs/Installation/terms-of-use.md" >}})
    - [Preface]({{< relref "/docs/Installation/preface.md" >}})
    - [Installation Preparations]({{< relref "/docs/Installation/installation-preparations.md" >}})
        - [Obtaining the Installation Source]({{< relref "/docs/Installation/obtaining-the-installation-source.md" >}})
        - [Release Package Integrity Check]({{< relref "/docs/Installation/release-package-integrity-check.md" >}})
        - [Hardware Compatibility]({{< relref "/docs/Installation/hardware-compatibility.md" >}})
        - [Minimal Hardware Specifications]({{< relref "/docs/Installation/minimal-hardware-specifications.md" >}})

    - [Installation Mode]({{< relref "/docs/Installation/installation-mode.md" >}})
        - [Installation Through the CD/DVD-ROM]({{< relref "/docs/Installation/installation-through-the-cd-dvd-rom.md" >}})

    - [Installation Guideline]({{< relref "/docs/Installation/installation-guideline.md" >}})
        - [Starting the Installation]({{< relref "/docs/Installation/starting-the-installation.md" >}})
        - [Using GUI Mode for Installation]({{< relref "/docs/Installation/using-gui-mode-for-installation.md" >}})
            - [Configuring an Installation Program Language]({{< relref "/docs/Installation/configuring-an-installation-program-language.md" >}})
            - [Entering the Installation Interface]({{< relref "/docs/Installation/entering-the-installation-interface.md" >}})
            - [Setting Installation Parameters]({{< relref "/docs/Installation/setting-installation-parameters.md" >}})
                - [Setting the Keyboard Layout]({{< relref "/docs/Installation/setting-the-keyboard-layout.md" >}})
                - [Setting a System Language]({{< relref "/docs/Installation/setting-a-system-language.md" >}})
                - [Setting Date and Time]({{< relref "/docs/Installation/setting-date-and-time.md" >}})
                - [Setting the Installation Source]({{< relref "/docs/Installation/setting-the-installation-source.md" >}})
                - [Selecting Installation Software]({{< relref "/docs/Installation/selecting-installation-software.md" >}})
                - [Setting the Installation Destination]({{< relref "/docs/Installation/setting-the-installation-destination.md" >}})
                - [Setting the Network and Host Name]({{< relref "/docs/Installation/setting-the-network-and-host-name.md" >}})

            - [Starting Installation]({{< relref "/docs/Installation/starting-installation.md" >}})
            - [Configurations During Installation]({{< relref "/docs/Installation/configurations-during-installation.md" >}})
            - [Completing the Installation]({{< relref "/docs/Installation/completing-the-installation.md" >}})

        - [Using Text Mode for Installation]({{< relref "/docs/Installation/using-text-mode-for-installation.md" >}})
            - [Entering the Installation Interface]({{< relref "/docs/Installation/entering-the-installation-interface-0.md" >}})
            - [Setting Installation Parameters]({{< relref "/docs/Installation/setting-installation-parameters-1.md" >}})
                - [Configuring the System Language]({{< relref "/docs/Installation/configuring-the-system-language.md" >}})
                - [Configuring the Time Zone and NTP Service]({{< relref "/docs/Installation/configuring-the-time-zone-and-ntp-service.md" >}})
                - [Configuring the Installation Source]({{< relref "/docs/Installation/configuring-the-installation-source.md" >}})
                - [Selecting Installation Software]({{< relref "/docs/Installation/selecting-installation-software-2.md" >}})
                - [Configuring the Installation Location]({{< relref "/docs/Installation/configuring-the-installation-location.md" >}})
                - [Configuring the Network]({{< relref "/docs/Installation/configuring-the-network.md" >}})
                - [Setting the root User Password]({{< relref "/docs/Installation/setting-the-root-user-password.md" >}})
                - [Creating a User]({{< relref "/docs/Installation/creating-a-user.md" >}})

            - [Completing the Installation]({{< relref "/docs/Installation/completing-the-installation-3.md" >}})


    - [FAQs]({{< relref "/docs/Installation/faqs.md" >}})
        - [Why Does openEuler Fail to Start After I Install It to the Second Disk?]({{< relref "/docs/Installation/why-does-openeuler-fail-to-start-after-i-install-it-to-the-second-disk.md" >}})
        - [What Are the Constraints on Network Configurations?]({{< relref "/docs/Installation/what-are-the-constraints-on-network-configurations.md" >}})
        - [Why Does openEuler Enter Emergency Mode After It Is Powered On?]({{< relref "/docs/Installation/why-does-openeuler-enter-emergency-mode-after-it-is-powered-on.md" >}})
        - [Failed to Reinstall openEuler When a Logical Volume Group That Cannot Be Activated Has Existed in openEuler]({{< relref "/docs/Installation/failed-to-reinstall-openeuler-when-a-logical-volume-group-that-cannot-be-activated-has-existed-in-op.md" >}})
        - [An Exception Occurs During the Selection of the Installation Source]({{< relref "/docs/Installation/an-exception-occurs-during-the-selection-of-the-installation-source.md" >}})
            - [Software Dependency]({{< relref "/docs/Installation/software-dependency.md" >}})

        - [How Do I Manually Enable the kdump Service?]({{< relref "/docs/Installation/how-do-i-manually-enable-the-kdump-service.md" >}})


