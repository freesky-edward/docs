# Setting Date and Time<a name="EN-US_TOPIC_0214071108"></a>

On the  **INSTALLATION SUMMARY**  page, click  **TIME & DATE**. On the  **TIME & DATE**  page, set the system time zone, date, and time.

When setting the time zone, you can click a specific city on the map with the mouse, or select a region from the drop-down list of  **Region**  or a city from the drop-down list of  **City**  at the top of the page, as shown in  [Figure 1](#en-us_topic_0186390096_en-us_topic_0122145900_fig1260162652312).

If your city is not displayed on the map or in the drop-down list, select the nearest city in the same time zone.

**Figure  1**  Setting date and time<a name="en-us_topic_0186390096_en-us_topic_0122145900_fig1260162652312"></a>  
![](figures/setting-date-and-time.png "setting-date-and-time")

After the setting is complete, click  **Done**  in the upper left corner to go back to the  **INSTALLATION SUMMARY**  page.

  

