# Installation Mode<a name="EN-US_TOPIC_0214071123"></a>

>![](public_sys-resources/icon-notice.gif) **NOTICE:**   
>The operating system can only be deployed on TaiShan 200 servers. For details about the types of supported servers, see  **Installation Preparations**  \>  **Hardware Compatibility**. Moreover, it can only be installed using the CD/DVD-ROM.  
 


