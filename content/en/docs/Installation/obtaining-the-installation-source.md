# Obtaining the Installation Source<a name="EN-US_TOPIC_0219108824"></a>

Obtain the openEuler release package and verification file before the installation.

Perform the following operations to obtain the openEuler release package:

1.  Log in to the  [openEuler Community](https://openeuler.org)  website.
2.  Click  **Download**. The download list is displayed.
3.  Click  **Get openEuler-1.0-aarch64-dvd.iso**  to download the  **openEuler-1.0-aarch64-dvd.iso**  release package to the local PC.
4.  Click the  **openEuler-1.0-aarch64-dvd.iso.sha256sum**  link under the  **please download the checksum file:**  area to download the  **openEuler-1.0-aarch64-dvd.iso.sha256sum**  verification file to the local PC.

