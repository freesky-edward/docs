# Completing the Installation<a name="EN-US_TOPIC_0214071172"></a>

openEuler has been installed, as shown in  [Figure 1](#en-us_topic_0186390267_en-us_topic_0122145917_fig1429512116338). Click  **Reboot**  to restart the system.

**Figure  1**  Completing the installation<a name="en-us_topic_0186390267_en-us_topic_0122145917_fig1429512116338"></a>  
![](figures/completing-the-installation.png "completing-the-installation")

>![](public_sys-resources/icon-note.gif) **NOTE:**   
>Remove the CD-ROM manually if it does not eject automatically during rebooting.  

After the restart is complete, log in to openEuler through the CLI.

