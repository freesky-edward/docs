# Completing the Installation<a name="EN-US_TOPIC_0214071125"></a>

After all mandatory items are configured on the installation overview page, the alarm symbols  **\[!\]**  will disappear. In this case, enter  **b**  to install the system.

**Figure  1**  Starting installation<a name="fig17675194210612"></a>  
![](figures/starting-installation-2.png "starting-installation-2")

**Figure  2**  Pressing  **Enter**  to restart the system after the installation is complete<a name="fig9722125434418"></a>  
![](figures/pressing-enter-to-restart-the-system-after-the-installation-is-complete.png "pressing-enter-to-restart-the-system-after-the-installation-is-complete")

**Figure  3**  Entering the login page after the installation is complete<a name="fig12380105124511"></a>  
![](figures/entering-the-login-page-after-the-installation-is-complete.png "entering-the-login-page-after-the-installation-is-complete")

>![](public_sys-resources/icon-note.gif) **NOTE:**   
>-   Because the BIOS reserves memory, the total memory \(indicated by  **MemTotal**\) is slightly different before and after the system restart.  
>-   After the system is installed, the kdump function is disabled by default. To use the kdump function, manually enable it. For details, see  **FAQs \> How Do I Manually Enable the kdump Service?**  

