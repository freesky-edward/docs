# 硬件兼容支持<a name="ZH-CN_TOPIC_0229291190"></a>

openEuler安装时，应注意硬件兼容性方面的问题，当前已支持的服务器类型如[表1](#table14948632047)所示。

>![](public_sys-resources/icon-note.gif) **说明：**   
>-   TaiShan 200服务器基于华为鲲鹏920处理器。  
>-   当前仅支持华为TaiShan服务器和FusionServer Pro 机架服务器，后续将逐步增加对其他厂商服务器的支持。  

**表 1**  支持的服务器类型

<a name="table14948632047"></a>
<table><thead align="left"><tr id="row5949431547"><th class="cellrowborder" valign="top" width="26.369999999999997%" id="mcps1.2.4.1.1"><p id="p694923843"><a name="p694923843"></a><a name="p694923843"></a><strong id="b1990114912213"><a name="b1990114912213"></a><a name="b1990114912213"></a>服务器形态</strong></p>
</th>
<th class="cellrowborder" valign="top" width="24%" id="mcps1.2.4.1.2"><p id="p132705020556"><a name="p132705020556"></a><a name="p132705020556"></a><strong id="b18630171675"><a name="b18630171675"></a><a name="b18630171675"></a>服务器名称</strong></p>
</th>
<th class="cellrowborder" valign="top" width="49.63%" id="mcps1.2.4.1.3"><p id="p119491131848"><a name="p119491131848"></a><a name="p119491131848"></a><strong id="b209018491722"><a name="b209018491722"></a><a name="b209018491722"></a>服务器型号</strong></p>
</th>
</tr>
</thead>
<tbody><tr id="row8949153348"><td class="cellrowborder" valign="top" width="26.369999999999997%" headers="mcps1.2.4.1.1 "><p id="p4893192424117"><a name="p4893192424117"></a><a name="p4893192424117"></a>机架服务器</p>
</td>
<td class="cellrowborder" rowspan="1" valign="top" width="24%" headers="mcps1.2.4.1.2 "><p id="p02706012553"><a name="p02706012553"></a><a name="p02706012553"></a>TaiShan 200</p>
</td>
<td class="cellrowborder" valign="top" width="49.63%" headers="mcps1.2.4.1.3 "><p id="p126551941225"><a name="p126551941225"></a><a name="p126551941225"></a>2280均衡型</p>
</td>
</tr>
<tr id="row104064391909"><td class="cellrowborder" valign="top" width="26.369999999999997%" headers="mcps1.2.4.1.1 "><p id="p54061539609"><a name="p54061539609"></a><a name="p54061539609"></a><span id="ph643711543013"><a name="ph643711543013"></a><a name="ph643711543013"></a>机架服务器</span></p>
</td>
<td class="cellrowborder" valign="top" width="24%" headers="mcps1.2.4.1.2 "><p id="p9937105713311"><a name="p9937105713311"></a><a name="p9937105713311"></a><span id="ph103021851548"><a name="ph103021851548"></a><a name="ph103021851548"></a>FusionServer Pro 机架服务器</span></p>
</td>
<td class="cellrowborder" valign="top" width="49.63%" headers="mcps1.2.4.1.3 "><p id="p154063394011"><a name="p154063394011"></a><a name="p154063394011"></a><span id="ph1569451019115"><a name="ph1569451019115"></a><a name="ph1569451019115"></a>FusionServer Pro 2288H V5</span></p>
<div class="note" id="note2046771016316"><a name="note2046771016316"></a><a name="note2046771016316"></a><span class="notetitle"> 说明： </span><div class="notebody"><p id="p1646710101130"><a name="p1646710101130"></a><a name="p1646710101130"></a>服务器要求配置Avago 3508 RAID控制卡和启用LOM-X722网卡。</p>
</div></div>
</td>
</tr>
</tbody>
</table>

