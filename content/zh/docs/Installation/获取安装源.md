# 获取安装源<a name="ZH-CN_TOPIC_0229291237"></a>

在安装开始前，您需要获取openEuler的发布包和校验文件。

请按以下步骤获取openEuler的发布包和校验文件：

1.  登录[openEuler社区](https://openeuler.org)网站。
2.  单击“下载”。
3.  单击“获取ISO：”后面的“Link”，显示版本列表。
4.  单击“openEuler-20.03-LTS”，进入openEuler 20.03 LTS版本下载列表。
5.  单击“ISO”，进入ISO下载列表。
    -   aarch64：AArch64架构的ISO。
    -   x86\_64：x86\_64架构的ISO。
    -   source：openEuler源码ISO。

6.  根据实际待安装环境的架构选择需要下载的openEuler的发布包和校验文件。
    -   若为AArch64架构。
        1.  单击“aarch64”。
        2.  单击“openEuler-20.03-LTS-aarch64-dvd.iso”，将openEuler发布包下载到本地。
        3.  单击“openEuler-20.03-LTS-aarch64-dvd.iso.sha256sum”，将openEuler校验文件下载到本地。

    -   若为x86\_64架构。
        1.  单击“x86\_64”。
        2.  单击“openEuler-20.03-LTS-x86\_64-dvd.iso”，将openEuler发布包下载到本地。
        3.  单击“openEuler-20.03-LTS-x86\_64-dvd.iso.sha256sum”，将openEuler校验文件下载到本地。

