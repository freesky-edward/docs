# 通过私有镜像安装<a name="ZH-CN_TOPIC_0229424319"></a>

本节介绍如何使用或者制作私有镜像，并介绍相应的操作步骤，指导用户进行安装。

## 制作私有镜像<a name="zh-cn_topic_0022605796_zh-cn_topic_0016259799_section66369966101113"></a>

制作私有镜像的方法请参见[《镜像服务用户指南》](https://support.huaweicloud.com/usermanual-ims/zh-cn_topic_0013901628.html)。

## 启动安装<a name="zh-cn_topic_0022605796_zh-cn_topic_0016259799_section47344128153516"></a>

华为公有云的x86虚拟化平台的启动请参见[ 弹性云服务器 ECS的用户指南](https://support.huaweicloud.com/wtsnew-ecs/index.html)。

