# 从服务端实时获取事件消息<a name="ZH-CN_TOPIC_0231454827"></a>

## 描述<a name="zh-cn_topic_0231454831_section13350115135310"></a>

isula events用于从服务端实时获取容器镜像生命周期、运行等事件消息, 仅支持runtime类型为lcr的容器。

## 用法<a name="zh-cn_topic_0231454831_section188811239165314"></a>

```
isula events [OPTIONS]
```

## 参数<a name="zh-cn_topic_0231454831_section4322824135919"></a>

<a name="zh-cn_topic_0231454831_table45852013111514"></a>
<table><tbody><tr id="zh-cn_topic_0231454831_row1790211601513"><td class="cellrowborder" valign="top" width="17.333333333333336%"><p id="zh-cn_topic_0231454831_p7179821161516"><a name="zh-cn_topic_0231454831_p7179821161516"></a><a name="zh-cn_topic_0231454831_p7179821161516"></a><strong id="zh-cn_topic_0231454831_b91798219151"><a name="zh-cn_topic_0231454831_b91798219151"></a><a name="zh-cn_topic_0231454831_b91798219151"></a>命令</strong></p>
</td>
<td class="cellrowborder" valign="top" width="39.57575757575758%"><p id="zh-cn_topic_0231454831_p15179121111511"><a name="zh-cn_topic_0231454831_p15179121111511"></a><a name="zh-cn_topic_0231454831_p15179121111511"></a><strong id="zh-cn_topic_0231454831_b717982112150"><a name="zh-cn_topic_0231454831_b717982112150"></a><a name="zh-cn_topic_0231454831_b717982112150"></a>参数</strong></p>
</td>
<td class="cellrowborder" valign="top" width="43.09090909090909%"><p id="zh-cn_topic_0231454831_p10180152151511"><a name="zh-cn_topic_0231454831_p10180152151511"></a><a name="zh-cn_topic_0231454831_p10180152151511"></a><strong id="zh-cn_topic_0231454831_b718015216152"><a name="zh-cn_topic_0231454831_b718015216152"></a><a name="zh-cn_topic_0231454831_b718015216152"></a>说明</strong></p>
</td>
</tr>
<tr id="zh-cn_topic_0231454831_row89859561117"><td class="cellrowborder" rowspan="3" valign="top" width="17.333333333333336%"><p id="zh-cn_topic_0231454831_p69851856411"><a name="zh-cn_topic_0231454831_p69851856411"></a><a name="zh-cn_topic_0231454831_p69851856411"></a>events</p>
</td>
<td class="cellrowborder" valign="top" width="39.57575757575758%"><p id="zh-cn_topic_0231454831_p549293210212"><a name="zh-cn_topic_0231454831_p549293210212"></a><a name="zh-cn_topic_0231454831_p549293210212"></a>-H, --host</p>
</td>
<td class="cellrowborder" valign="top" width="43.09090909090909%"><p id="zh-cn_topic_0231454831_p1049213321528"><a name="zh-cn_topic_0231454831_p1049213321528"></a><a name="zh-cn_topic_0231454831_p1049213321528"></a>指定要连接的iSulad socket文件路径</p>
</td>
</tr>
<tr id="zh-cn_topic_0231454831_row287455224012"><td class="cellrowborder" valign="top"><p id="zh-cn_topic_0231454831_p687465212409"><a name="zh-cn_topic_0231454831_p687465212409"></a><a name="zh-cn_topic_0231454831_p687465212409"></a>-n, --name</p>
</td>
<td class="cellrowborder" valign="top"><p id="zh-cn_topic_0231454831_p1087455204011"><a name="zh-cn_topic_0231454831_p1087455204011"></a><a name="zh-cn_topic_0231454831_p1087455204011"></a>获取指定容器的事件消息</p>
</td>
</tr>
<tr id="zh-cn_topic_0231454831_row59371553428"><td class="cellrowborder" valign="top"><p id="zh-cn_topic_0231454831_p199381955421"><a name="zh-cn_topic_0231454831_p199381955421"></a><a name="zh-cn_topic_0231454831_p199381955421"></a>-S, --since</p>
</td>
<td class="cellrowborder" valign="top"><p id="zh-cn_topic_0231454831_p16938145144210"><a name="zh-cn_topic_0231454831_p16938145144210"></a><a name="zh-cn_topic_0231454831_p16938145144210"></a>获取指定时间以来的事件消息</p>
</td>
</tr>
</tbody>
</table>

## 示例<a name="zh-cn_topic_0231454831_section1734193235916"></a>

从服务端实时获取事件消息，命令示例如下：

```
$ isula events
```

