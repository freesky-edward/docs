# 加固生效<a name="ZH-CN_TOPIC_0225297203"></a>

完成修改usr-security.conf文件后，请运行如下命令使新添加的配置生效。

```
systemctl restart openEuler-security.service
```

