# 启动数据库<a name="ZH-CN_TOPIC_0230050750"></a>

1.  启动PostgreSQL数据库。

    ```
    $/usr/bin/pg_ctl -D /data/ -l /data/logfile start
    ```

2.  确认PostgreSQL数据库进程是否正常启动。

    ```
    $ps -ef | grep postgres
    ```

    命令执行后，打印信息如下图所示，PostgreSQL相关进程已经正常启动了。

    ![](figures/postgres.png)


