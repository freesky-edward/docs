# 运行<a name="ZH-CN_TOPIC_0230408666"></a>

1.  开启mariadb服务器。

    ```
    #systemctl start mariadb
    ```

2.  <a name="li197143190587"></a>初始化数据库。

    ```
    #/usr/bin/mysql_secure_installation
    ```

    命令执行过程中需要输入数据库的root设置的密码，若没有密码则直接按“Enter”。然后根据提示及实际情况进行设置。

3.  登录数据库。

    ```
    # mysql -u root -p
    ```

    命令执行后提示输入密码。密码为[2](#li197143190587)中设置的密码。

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >执行**\\q**或者**exit**可退出数据库。  


