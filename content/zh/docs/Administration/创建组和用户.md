# 创建组和用户<a name="ZH-CN_TOPIC_0230050743"></a>

>![](public_sys-resources/icon-note.gif) **说明：**   
>服务器环境下，为了系统安全，通常会为进程分配单独的用户，以实现权限隔离。本章节创建的组和用户都是操作系统层面的，不是数据库层面的。  

1.  创建PostgreSQL用户（组）。

    ```
    #groupadd  postgres
    ```

    ```
    #useradd  -g postgres postgres
    ```

2.  设置postgres用户密码（重复输入密码）。

    ```
    #passwd postgres
    ```


