# 选择数据库<a name="ZH-CN_TOPIC_0231470885"></a>

一般创建表，查询表等操作首先需要选择一个目标数据库。可以使用USE语句来选择数据库。

```
USE databasename;
```

其中：databasename为数据库名称。

## 示例<a name="section207605920321"></a>

\#选择databaseexample数据库。

```
> USE databaseexample;
```

