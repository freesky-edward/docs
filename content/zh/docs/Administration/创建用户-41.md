# 创建用户<a name="ZH-CN_TOPIC_0231470877"></a>

可以使用CREATE USER语句来创建一个或多个用户，并设置相应的口令。

```
CREATE USER 'username'@'hostname' IDENTIFIED BY 'password';
```

其中：

-   username：用户名。
-   host：主机名，即用户连接数据库时所在的主机的名字。若是本地用户可用localhost，若在创建的过程中，未指定主机名，则主机名默认为“%”，表示一组主机。
-   password：用户的登陆密码，密码可以为空，如果为空则该用户可以不需要密码登陆服务器，但从安全的角度而言，不推荐这种做法。

使用CREATE USER语句必须拥有数据库的INSERT权限或全局CREATE USER权限。

使用CREATE USER语句创建一个用户账号后，会在系统自身的数据库的user表中添加一条新记录。若创建的账户已经存在，则语句执行时会出现错误。

新创建的用户拥有的权限很少，只允许进行不需要权限的操作，如使用SHOW语句查询所有存储引擎和字符集的列表等。

## 示例<a name="section207605920321"></a>

\#创建密码为123456，用户名为userexample1的本地用户。

```
> CREATE USER 'userexample1'@'localhost' IDENTIFIED BY '123456';
```

\#创建密码为123456，用户名为userexample2，主机名为192.168.1.100的用户。

```
> CREATE USER 'userexample2'@'192.168.1.100' IDENDIFIED BY '123456';
```

