# 查看用户<a name="ZH-CN_TOPIC_0231470878"></a>

可以使用SHOW GRANTS语句或SELECT语句查看一个或多个用户。

查看特定用户：

```
SHOW GRANTS [FOR 'username'@'hostname'];
```

```
SELECT USER,HOST,PASSWORD FROM mysql.user WHERE USER='username';
```

查看所有用户：

```
SELECT USER,HOST FROM mysql.user;
```

其中：

-   username：用户名。
-   hostname：主机名。

## 示例<a name="section207605920321"></a>

\#查看userexample1用户。

```
> SHOW GRANTS FOR 'userexample1'@'localhost';
```

\#查看mysql数据库中所有用户。

```
> SELECT USER,HOST FROM mysql.user;
```

