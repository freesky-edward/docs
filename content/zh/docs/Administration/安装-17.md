# 安装<a name="ZH-CN_TOPIC_0230408665"></a>

1.  配置本地yum源，详细信息请参考[搭建repo服务器](搭建repo服务器.md)。
2.  清除缓存。

    ```
    #dnf clean all
    ```

3.  创建缓存。

    ```
    #dnf makecache
    ```

4.  安装mariadb服务器。

    ```
    #dnf install mariadb-server
    ```

5.  查看安装后的rpm包。

    ```
    #rpm -qa | grep mariadb
    ```


