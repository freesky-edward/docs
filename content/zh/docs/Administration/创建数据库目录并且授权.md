# 创建数据库目录并且授权<a name="ZH-CN_TOPIC_0230704897"></a>

1.  在[搭建数据盘](搭建数据盘-15.md)已创建的数据目录**/data**基础上，继续创建进程所需的相关目录并授权MySQL用户（组）。

    ```
    #mkdir -p /data/mariadb
    #cd /data/mariadb
    #mkdir data tmp run log
    #chown -R mysql:mysql /data
    ```


