# 删除数据库<a name="ZH-CN_TOPIC_0231470887"></a>

可以使用DROP DATABASE语句来删除数据库。

>![](public_sys-resources/icon-caution.gif) **注意：**   
>删除数据库要谨慎操作，一旦删除，数据库中的所有表和数据都会删除。  

```
DROP DATABASE databasename;
```

其中：databasename为数据库名称。

DROP DATABASE命令用于删除创建过\(已存在\)的数据库，且会删除数据库中的所有表，但数据库的用户权限不会自动删除。

要使用DROP DATABASE，您需要数据库的DROP权限。

DROP SCHEMA是DROP DATABASE的同义词。

## 示例<a name="section207605920321"></a>

\#删除databaseexample数据库。

```
> DROP DATABASE databaseexample;
```

