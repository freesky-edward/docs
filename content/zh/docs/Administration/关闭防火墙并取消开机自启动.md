# 关闭防火墙并取消开机自启动<a name="ZH-CN_TOPIC_0230050741"></a>

>![](public_sys-resources/icon-note.gif) **说明：**   
>测试环境下通常会关闭防火墙以避免部分网络因素影响，视实际需求做配置。  

1.  停止防火墙。

    ```
    #systemctl stop firewalld
    ```

2.  关闭防火墙。

    ```
    #systemctl disable firewalld
    ```

    >![](public_sys-resources/icon-note.gif) **说明：**   
    >执行disable命令关闭防火墙的同时，也取消了开机自启动。  


