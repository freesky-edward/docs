# 创建数据库<a name="ZH-CN_TOPIC_0230590568"></a>

可以使用CREATE DATABASE语句或createdb来创建角色。createrdb是对CREATE DATABASE命令的封装，需要在shell界面执行，而不是在数据库界面。

```
CREATE DATABASE databasename;
```

```
createdb databasename
```

其中：databasename为数据库名。

要使用这条命令，必须拥有CREATEDB权限。

## 示例<a name="section207605920321"></a>

\#创建一个数据库database1。

```
postgres=# CREATE DATABASE database1;
```

