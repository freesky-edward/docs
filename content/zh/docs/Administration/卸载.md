# 卸载<a name="ZH-CN_TOPIC_0230050756"></a>

1.  在postgres用户下停止数据库。

    ```
    $/usr/bin/pg_ctl -D /data/ -l /data/logfile stop
    ```

2.  在root用户下执行**dnf remove postgresql-server**卸载PostgreSQL数据库。

    ```
    #dnf remove postgresql-server
    ```


