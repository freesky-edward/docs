# 管理虚拟机USB<a name="ZH-CN_TOPIC_0225484547"></a>

为了方便在虚拟机内部使用USBkey设备、USB海量存储设备等USB设备，openEuler提供了USB设备直通的功能。用户可以通过USB直通和热插拔相关接口给虚拟机配置直通USB设备、或者在虚拟机处于运行的状态下热插/热拔USB设备。
