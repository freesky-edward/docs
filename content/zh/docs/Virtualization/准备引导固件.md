# 准备引导固件<a name="ZH-CN_TOPIC_0184192548"></a>

## 概述<a name="section174915189245"></a>

针对不同的架构，引导的方式有所差异。x86支持UFEI（Unified Extensible Firmware Interface）和BIOS方式启动，AArch64仅支持UFEI方式启动。openEuler默认已安装BIOS启动对应的引导文件，不需要用户额外操作。所以这里仅介绍UEFI启动方式的安装方法。

统一的可扩展固件接口UEFI是一种全新类型的接口标准，用于开机自检、引导操作系统的启动，是传统BIOS的一种替代方案。EDK II是一套实现了UEFI标准的开源代码，在虚拟化场景中，通常利用EDK II工具集，通过UEFI的方式启动虚拟机。使用EDK II工具需要在虚拟机启动之前安装对应的软件包 ，本节介绍EDK II的安装方法。

## 安装方法<a name="section86091043192617"></a>

如果使用UEFI方式引导，需要安装工具集EDK II，AArch64架构对应的安装包为edk2-aarch64，x86架构对应的安装包为edk2-ovmf。这里以AArch64架构为例，给出具体的安装方法，x86架构仅需将edk2-aarch64替换为edk2-ovmf。

1.  安装edk软件包，命令如下：

    在AArch64架构下edk2的包名为edk2-aarch64

    ```
    # yum install -y edk2-aarch64
    ```

    在x86\_64架构下edk2的包名为edk2-ovmf

    ```
    # yum install -y edk2-ovmf
    ```

2.  查询edk软件是否安装成功，命令如下：

    在AArch64架构下查询如下

    ```
    # rpm -qi edk2-aarch64
    ```

    若edk软件安装成功，回显类似如下：

    ```
    Name        : edk2-aarch64
    Version     : 20180815gitcb5f4f45ce
    Release     : 1.oe3
    Architecture: noarch
    Install Date: Mon 22 Jul 2019 04:52:33 PM CST
    Group       : Applications/Emulators
    ```

    在x86\_64架构下查询如下

    ```
    # rpm -qi edk2-ovmf
    ```

    若edk软件安装成功，回显类似如下：

    ```
    Name        : edk2-ovmf
    Version     : 201908
    Release     : 6.oe1
    Architecture: noarch
    Install Date: Thu 19 Mar 2020 09:09:06 AM CST
    ```


