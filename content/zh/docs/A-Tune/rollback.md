# rollback<a name="ZH-CN_TOPIC_0213225930"></a>

## 功能描述<a name="section124121426195015"></a>

回退当前的配置到系统的初始配置。

## 命令格式<a name="section1019897115110"></a>

**atune-adm rollback**

## 使用示例<a name="section5961238145111"></a>

```
# atune-adm rollback
```

