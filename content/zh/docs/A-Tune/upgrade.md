# upgrade<a name="ZH-CN_TOPIC_0213225931"></a>

## 功能描述<a name="section124121426195015"></a>

更新系统的数据库。

## 命令格式<a name="section1019897115110"></a>

**atune-adm upgrade**  <DB\_FILE\>

## 参数说明<a name="section19419181017266"></a>

-   DB\_FILE

    新的数据库文件路径


## 使用示例<a name="section5961238145111"></a>

数据库更新为new\_sqlite.db。

```
# atune-adm upgrade ./new_sqlite.db
```

