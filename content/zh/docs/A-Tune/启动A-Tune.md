# 启动A-Tune<a name="ZH-CN_TOPIC_0213178457"></a>

A-Tune安装完成后，需要启动A-Tune服务才能使用。

-   启动atuned服务：

    ```
    # systemctl start atuned
    ```


-   查询atuned服务状态：

    ```
    # systemctl status atuned
    ```

    若回显为如下，则服务启动成功。

    ![](figures/zh-cn_image_0214540398.png)


