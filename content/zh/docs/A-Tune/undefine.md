# undefine<a name="ZH-CN_TOPIC_0213225907"></a>

## 功能描述<a name="section124121426195015"></a>

删除用户自定义的workload\_type。

## 命令格式<a name="section1019897115110"></a>

**atune-adm undefine**  <WORKLOAD\_TYPE\>

## 使用示例<a name="section5961238145111"></a>

删除自定义的负载类型test\_type。

```
# atune-adm undefine test_type 
```

