# 下载并运行IntelliJ IDEA<a name="ZH-CN_TOPIC_0229243682"></a>

在执行如上环境配置后，您就可以下载使用IntelliJ IDEA了。鉴于最新版的IntelliJ IDEA和openEuler系统在部分功能上有兼容性问题，建议您从此[链接](https://www.jetbrains.com/idea/download/other.html)下载2018版本linux压缩包。下载好后把压缩包移到您想要安装该软件的目录，对压缩包进行解压：

```
# tar xf ideaIC-2018.3.tar.gz
```

解压后切换到IntelliJ IDEA的目录下并运行。

```
# cd ./idea-IC-183.4284.148
# bin/idea.sh &
```

