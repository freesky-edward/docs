# 使用MobaXterm登录服务器<a name="ZH-CN_TOPIC_0229243732"></a>

MobaXterm是一款非常优秀的SSH客户端，其自带X Server，可以轻松解决远程GUI显示问题。

您需要提前下载安装好MobaXterm并打开，然后SSH登录您的服务器并进行以下操作。

