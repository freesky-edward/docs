# java程序生成流程<a name="ZH-CN_TOPIC_0229243688"></a>

通过JDK将java源代码文件生成并运行Java程序，需要经过编译和运行。

1.  编译：是指使用Java编译器（javac）将java源代码文件（.java文件）编译为.class的字节码文件。
2.  运行：是指在Java虚拟机上执行字节码文件。

