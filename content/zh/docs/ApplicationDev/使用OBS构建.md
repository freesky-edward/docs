# 使用OBS构建<a name="ZH-CN_TOPIC_0229243715"></a>

本章介绍通过网页和osc构建RPM软件包的方法。主要包括如下两类：

-   修改已有软件包：修改已有软件包源代码，然后将修改后的源代码构建成一个RPM软件包。
-   新增软件包：从无到有全新开发一个新的软件源文件，并将新开发的源文件构建成一个RPM软件包。


