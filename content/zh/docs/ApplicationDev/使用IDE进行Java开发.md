# 使用IDE进行Java开发<a name="ZH-CN_TOPIC_0229243717"></a>

对于小型的Java程序，可以直接[使用JDK编译](使用JDK编译.md)得到可运行Java应用。但是对于大中型Java应用，这种方式已经无法满足开发者的需求。因此您可以参考如下步骤安装IDE并进行使用，以方便您在openEuler系统上的Java开发工作。



